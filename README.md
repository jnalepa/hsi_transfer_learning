# Transfer Learning for Segmenting Dimensionally-Reduced Hyperspectral Images
## Jakub Nalepa, Member, IEEE, Michal Myller, and Michal Kawulok, Member, IEEE

This repository provides supplementary material to the paper listed above (submitted to the IEEE Geoscience and Remote Sensing Letters journal). It includes:

- **Example visualizations** (the /visualizations folder) of the segmentation results obtained using the 1D-CNN and PT-CNN deep models trained in all investigated settings. For each model (1D-CNN and PT-CNN), for each architectural design (one, two, or three building blocks), and for each number of investigated bands (either original HSI or simulated bands), we render the resulting visualization, obtained out of 25 executions (each pixel is annotated with a class label which was the most frequently assigned to this pixel within all 25 independent runs).

- **Code** alongside an **example Jupyter notebook** (/code)
    - *band_mapper.py*&mdash;Class implementing an approach for mapping HSI to a specific number of simulated bands.
    - *dataset_structures.py*&mdash;Classes used for efficient handling and pre-processing of high-dimensional data (here, HSI).
    - *keras_models.py*&mdash;The deep models (both our 1D-CNN and the state-of-the-art PT-CNN) implemented in Keras.
    - *load_data.py*&mdash;I/O routines.
    - *example.ipynb*&mdash;An example showing the full procedure of training the extractor on one (source) dataset and then using it to classify a different (target) one.

- The **supplementary material** (*2019_Transfer_learning_for_HSI_segmentation_supplement.pdf*) which includes:
    - Table of symbols used in this letter.
    - Table of abbreviations used in this letter.
    - Example visualiations.
    - Detailed statistical results (two-tailed Wilcoxon tests).