from typing import Tuple, NamedTuple
from keras.models import Sequential
from keras.optimizers import Adam
from keras.layers import Flatten, Conv1D, MaxPooling1D, Dense, BatchNormalization


class ModelSettings(NamedTuple):
    input_neighbourhood: Tuple[int, int]
    first_conv_kernel_size: Tuple[int, int]


def build_1d_cnn(input_shape, filters, kernel_size, classes_count, blocks=1):
    optimizer = Adam(lr=0.0001)

    def add_block(model):
        model.add(
            Conv1D(filters, kernel_size, padding="valid", activation='relu'))
        model.add(BatchNormalization())
        model.add(MaxPooling1D(pool_size=2))
        return model

    model = Sequential()
    model.add(Conv1D(filters, kernel_size, input_shape=input_shape, padding="valid", activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling1D(pool_size=2))
    if blocks > 1:
        for block in range(blocks - 1):
            model = add_block(model)
    model.add(Flatten())
    model.add(Dense(units=512, activation='relu'))
    model.add(Dense(units=128, activation='relu'))
    model.add(Dense(units=classes_count, activation='softmax'))
    model.compile(optimizer=optimizer,
                  metrics=['accuracy'],
                  loss='categorical_crossentropy')
    return model


def build_pt_cnn(input_shape, kernel_size, classes_count, blocks=1):

    def add_second_block(model):
        model.add(Conv1D(filters=10, kernel_size=30, activation='relu'))
        model.add(BatchNormalization())
        return model

    def add_third_block(model):
        model.add(Conv1D(filters=10, kernel_size=10, activation='relu'))
        model.add(BatchNormalization())
        return model

    optimizer = Adam(lr=0.0001)

    model = Sequential()
    model.add(Conv1D(kernel_size=kernel_size, filters=20, input_shape=input_shape, padding="valid", activation='relu'))
    model.add(BatchNormalization())
    if blocks > 1:
        model = add_second_block(model)
    if blocks > 2:
        model = add_third_block(model)

    model.add(Flatten())
    model.add(Dense(units=20, activation='relu'))
    model.add(Dense(units=20, activation='relu'))
    model.add(Dense(units=classes_count, activation='softmax'))
    model.compile(optimizer=optimizer, metrics=['accuracy'],
                  loss='categorical_crossentropy')
    return model
